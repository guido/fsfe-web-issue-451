What is this and what does it do?
=================================

In this repository, there are some scripts that may help coordinators of
[FSFE local groups](https://wiki.fsfe.org/LocalGroups) to promote their
events on the pages of [FSFE](https://fsfe.org/events) and elsewhere.

It got its weird name from [ticket
451](https://trac.fsfe.org/fsfe-web/ticket/451) in FSFE's tracker.

As regular meetings usually require regular announcements, the wording
is mostly the same. The concept is that there is one file which contains
variables (e.g. date, time, location) and a few templates where these
variables are inserted and a few scripts that publish the snippets for
the particular event.

Once everything is set up, the file with the variables (corefacts) can
be modified and the scripts be fired off with one command which will
result to:

-   publishing the event and pre-defined translations of it on fsfe.org
-   sent periodic reminders per email or social media automatically at
    predefined times relative to the event.

The following has been tested on a fresh Debian installation
(debian~version~: 8.4) with internet connectivity and should thus work
on any Debian based system and probably any other \*nix machine.

Requirements
============

write access to FSFE
--------------------

You need write access to [FSFE's svn
repository](http://fsfe.org/contribute/web/web) to make use of this
script. I asked fellowship hackers to create an extra account with
access to the events directory only. You may consider a similar step.

Packages: subversion and at
---------------------------

You'll need: subversion, at (and optional to conveniently get this and
keep updated: git).

``` {.example}
(sudo) apt-get install subversion at # git
```

These scripts
-------------

Either via git from repository:

``` {.example}
git clone git@git.fsfe.org:guido/fsfe-web-issue-451.git
```

or download tar.gz folder

``` {.example}
wget https://git.fsfe.org/guido/fsfe-web-issue-451/archive/master.tar.gz
```

and exctract it.

Setting up the stage
====================

Change to directory trunk

``` {.example}
cd fsfe-web-issue-451/trunk/
```

move/copy the "fsferm" directory to YOURGROUPNAME

``` {.example}
cp -r fsferm YOURGROUPNAME
```

You may want to edit the templates in YOURGROUPNAME/snippets and adopt
them to your needs. Feel free to delete some languages you don't want to
maintain.

install missing locales
-----------------------

(you may need to install locales utf-8 packages for the languages you
want to maintain

On Debian, you can do this as root:

``` {.example}
dpkg-reconfigure locales
```

and chose the languages you want to support. For FSFE webpages, it
should be utf-8 only. FSFERM currently uses:

``` {.example}
de_DE.UTF-8 
el_GR.UTF-8 
en_GB.UTF-8 
fr_FR.UTF-8 
nl_NL.UTF-8
```

edit SVN options section in "event-auto-commit.sh"
--------------------------------------------------

-   change \$basedir to dir that contains repo

``` {.example}
basedir="~/myfolder/fsfe-web-issue-451/trunk"
svnuser="YOUR-SVN-USERNAME"
```

### SVN URI - the root svn dir

see <http://fsfe.org/contribute/web/web.en.html>

you may be asked to verify the fingerprint of the certificate.

Configure subversion that you won't need to type in a password. It is
suggested to configure svn for password encryption according to the
[documentation](https://wiki.apache.org/subversion/EncryptedPasswordStorage).

Also possible, but least secure: put the plaintext password into the
script.

edit "corefacts"
----------------

open the file "corefacts" and edit according to planned event

``` {.example}
groupname=YOURGROUPNAME
```

It has to be the name you chose for the folder (previously fsferm) as
the script will look into that folder for snippets and other group
specific data

``` {.example}
dateofevent=20140903
```

The date of the event in Form YearMonthDay The sample shows: September
3rd, 2014

``` {.example}
meetingtime="19:00"
```

``` {.example}
location="Weinhaus Stegmann"
```

The name of the venue your event will take place

``` {.example}
city="London"
```

The name of the city or village your event will take place

Depending on your "snippets" this section may contain more or less
variables.

run the event-auto-commit script for the first time
===================================================

``` {.example}
./event-auto-commit.sh
```

from now on, you just need to change the variables in "corefacts" and
run event-auto-commit afterwards. It should just run through now without
any interaction and submit newly generated event entries to the svn.

If not, drop a note to coordinators@

running promo-event.sh
======================

when this script is executed, the data from corefacts will be used to
create tweets, dents, emails to be published/sent at pre-defined times
relative to the date of the event.

Currently, there are scripts for mail, Twitter and Diaspora:

``` {.example}
./YOURGROUPNAME/mailevent.sh  
./YOURGROUPNAME/tweetevent.sh 
./YOURGROUPNAME/diaspevent.sh 
```

Change the texts according to you needs.


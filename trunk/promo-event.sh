#!/bin/bash

# This script shall send reminder emails to a specified address
# It's currently hosted at https://github.com/Schwoggel/fsfe-web-issue-451
# Once everything works fine, it will be merged to the event-auto-commit script

# please enter the script's base directory
# TODO outsource as many variables as possible
basedir="/home/fsferm/fsfe-web-issue-451/trunk"


# Snippet group name
# I chose this format to allow more recurrent events to be implemented as talks, booths, etc.

source "$basedir/corefacts"

#create unified ID for jobs as corefacts may change before at jobs will be executed

if [ -e $basedir/$groupname/jobs/$groupname-$dateofevent ]
    then echo " Event already exists. Exiting..."
    exit 1
fi

# it should be now safe to go ahead
# preserve corefacts for future jobs while the file "corefacts" may be overwritten in the meantime.
cp $basedir/corefacts $basedir/$groupname/jobs/$groupname-$dateofevent

eventdata="$basedir/$groupname/jobs/$groupname-$dateofevent"


# TODO Check if all needed variables (date time location) are present and valid

# check if dateofevent is actually in the future
if [ $dateofevent -lt `date +%Y%m%d` ]; then
   echo "date of event is in the past" 
   exit 1 
   fi


#Things to do three weeks out
threeweeks=`date --date $dateofevent' '$meetingtime' 3 weeks ago 11 hours ago' +%Y%m%d%H%M`
if [ $threeweeks -gt `date +%Y%m%d%H%M` ]; then
   echo "more than 3 weeks out. Good." 
   # insert applicable commands here
	at -t $threeweeks <<< "$basedir/$groupname/mailevent.sh $eventdata"
	at -t $threeweeks <<< "$basedir/$groupname/queetevent.sh $eventdata"
   fi

#Things to do one week out
oneweek=`date --date $dateofevent' '$meetingtime' 1 week ago 11 hours ago' +%Y%m%d%H%M`
if [ $oneweek -gt `date +%Y%m%d%H%M` ]; then
   echo "more than 1 week out. Good." 
   # insert applicable commands here
	at -t $oneweek <<< "$basedir/$groupname/mailevent.sh $eventdata"
	at -t $oneweek <<< "$basedir/$groupname/queetevent.sh $eventdata"
   fi

#Things to do two days out
twodays=`date --date $dateofevent' '$meetingtime' 2 days ago 11 hours ago' +%Y%m%d%H%M`
if [ $twodays -gt `date +%Y%m%d%H%M` ]; then
   echo "more than two days out. Good." 
   # insert applicable commands here
	at -t $twodays <<< "$basedir/$groupname/mailevent.sh $eventdata"
	at -t $twodays <<< "$basedir/$groupname/queetevent.sh $eventdata"
   fi

#Things to do on same day
sameday=`date --date $dateofevent' '$meetingtime' 11 hours ago' +%Y%m%d%H%M`
if [ $sameday -gt `date  +%Y%m%d%H%M` ]; then
   echo "more than 1 day out. Good." 
   # insert applicable commands here
	at -t $sameday <<< "$basedir/$groupname/mailevent.sh $eventdata"
	at -t $sameday <<< "$basedir/$groupname/queetevent.sh $eventdata"
   fi


#testday=`date --date 'now +1 minute' +%Y%m%d%H%M`
#echo $testday

# for testing:
#at -t  $testday <<< "$basedir/$groupname/mailevent.sh $eventdata"
#at -t  $testday <<< "/home/guido/bin/dradio dkultur 2 testing-asdf"

exit 0

#!/bin/bash

mailaddress=rhein-main@lists.fsfe.org
senderaddress=guido@fsfe.org


# getting the variables
if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1
# TODO check if source file was actually useful.


# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))
 
# the subject to be sent:
mailsubject="Fellowship-Treffen Rhein/Main in $city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y`"

if [ $daysleft \< 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the subject to be sent:
mailsubject="Nicht vergessen: Fellowship-Treffen Rhein/Main in $city mit Vortrag!"
fi



mailbody="Hallo allerseits,

die FSFE FellowshipGruppe Rhein/Main trifft sich wieder am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` um $meetingtime Uhr im $location in $city. Diesmal mit einem Vortrag von $speaker mit dem Titel \"$talktitle\".

Alle an Freier Software Interessierte sind herzlich eingeladen!

Nähere Informationen finden sich wie immer auf der Wiki-Seite:
https://wiki.fsfe.org/LocalGroups/RheinMain

Vorschläge für die Agenda können auf dieser Wiki-Seite eingetragen werden:
https://wiki.fsfe.org/LocalGroups/RheinMain/Agenda


Viele Grüße

Guido


PS: Diese Nachricht wurde automatisch erstellt. Falls der Inhalt keinen Sinn
ergibt, oder sonstige Rückmeldungen bitte per Mail an mich. Danke!"


echo "$mailbody" | mail -a "Content-Type: text/plain; charset=UTF-8" -r "$senderaddress" -s "$mailsubject" "$mailaddress"

#echo "$mailbody"

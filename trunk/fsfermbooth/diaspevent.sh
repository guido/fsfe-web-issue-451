#!/bin/bash


#diaspaccount=fsferm@despora.de

diaspaccount=fsferm@cryptospora.net

if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1


diasptext="#FSFE Info-Stand auf $event in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` $eventurl"

if [ $daysleft -lt 6 ]
   then 
# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))

# the message to be sent:
diasptext="Noch $daysleft Tage bis zum #FSFE Info-Stand auf $event in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` $eventurl"
fi

if [ $daysleft -lt 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the message to be sent:
diasptext="Nicht mehr lange: #FSFE Info-Stand auf $event in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` $eventurl"  
fi


echo "$diasptext"  | cliaspora -a $diaspaccount post public 
#echo "$diasptext" 

exit 0

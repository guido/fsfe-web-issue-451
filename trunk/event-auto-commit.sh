#!/bin/bash

# This script is intended to generate the xml event entries on fsfe.org for
# upcoming Fellowship meetings.  It expects GNU date in order to skip the
# leading zero in dates. 

# the snippets and the translations may depend on certain locales packages to
# work right

# It's currently hosted at https://github.com/Schwoggel/fsfe-web-issue-451


############# Table of Contents ############# 
# 
# 1. General variables
# 2. SVN options
# 3. Debugging options ###
# 4. check if dateofevent is actually in the future
# 5. ensure svn is ready for us
# 6. Find filename for xml files
# 7. create xml files locally
# 8. commit files to svn server

############# ##### ## #######  ############# 


# 1. General variables

# we'll use absolute paths. Here's this script:
# TODO this can be found out automatically
basedir="/home/guido/fsfe/fsferm2/fsfe-web-issue-451/trunk"
groupname=fsferm
pathtogit="/home/guido/fsfe/fsfe-website"
eventdir=$pathtogit/events
source "$basedir/corefacts"
gituser="guido"



# Set the SVN commandline
[[ "$debug" != "1" ]] && svnquiet="-q"

### 3. Debugging options ###
# enable debug messages? 0 for off, 1 for on
debug=1

# Text to print at the beginning of debug output liness
debugtext="DEBUG:"


# 4. check if dateofevent is actually in the future
if [ $dateofevent -lt `date +%Y%m%d` ]; then
   echo "date of event is in the past" 
   exit 1 
   fi


# 5. Check if new Year
# check if the base directory exists and has an events subdirectory with the applicaple year

# Let's check out this year's events directory
[[ "$debug" == "1" ]] && echo "$debugtext Starting SVN checkout "

cd $pathtogit
mkdir -p events/`date -d $dateofevent +%Y`
cd $basedir

# 6. Find filename for xml files

[[ "$debug" == "1" ]] && echo "$debugtext Date for Event: $dateofevent"

# date for filename
filedate=$(date -d "$dateofevent" +%Y%m%d)
[[ "$debug" == "1" ]] && echo "$debugtext filedate: $filedate"

# Adapting commit message:
commitmsg="$commitmsg >>> Adds event for $groupname in $city on $filedate"
[[ "$debug" == "1" ]] && echo "$debugtext Commit message is now \"$commitmsg\""
# if $go is not zero, we can safely start writing the files without 
# overwriting old stuff
go=0

# $filecount is used for the filename. If there is already an event 
# on the date specified, we increase this until we have a "free" file name
filecount=1

# walk through all the languages available for the specified location
for lang in $(ls $basedir/$groupname/snippets | grep -v tags.xml); do

  # $finished is set to 1 after the file has been written; this does not happen 
  # before we don't have a usable serial number for our event entries
  finished=0
  while [ $finished -eq 0 ]; do

    # event serial number must have two digits
    if [ $filecount -lt 10 ]; then
      newcount="0$filecount"
    else
      newcount=$filecount
    fi

    # file name for our event
    file="$eventdir/`date -d $dateofevent +%Y`/event-$filedate-$newcount.$lang"

    # finds all files with the name we want to use
    todaycontent=$(find $basedir/ -type f | grep -v "\.git" | grep -i event-$filedate-$newcount)

    # if the list created above is *not* empty, we try the next serial number, 
    # because there's a file with that serial number in at least one language
    # otherwise, we set $go to 1 so we know it's save to start writing files
    if [ "$todaycontent" != "" ]; then
      [ $go -eq 0 ] && filecount=$[$filecount+1]
      [[ "$debug" == "1" ]] && echo "$debugtext File serial number raised to $filecount"
    else
      go=1
    fi
    
    # only write those files if $go has been set to 1!
    if [ $go -eq 1 ]; then

	# 7. create xml files locally
	$basedir/$groupname/snippets/$lang > $file

      finished=1
    fi
  done

done

# 8. commit files to svn server

branch="$groupname-event-update-$dateofevent"

# Let's do a checkin on the files
[[ "$debug" == "1" ]] && echo "$debugtext Starting git update"
cd $eventdir
git fetch upstream
git checkout master
git pull upstream master
git branch $branch
git checkout $branch
git add --all
git commit -m "$commitmsg"
git push --set-upstream origin $branch

echo "https://git.fsfe.org/FSFE/fsfe-website/compare/master...$gituser:$branch"

# TODO check if successful

echo "Automatic event entry for event in $groupname on $dateofevent has been commited to $svnuri."
echo "Visit http://fsfe.org to check the results."

exit 0
